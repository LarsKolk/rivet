# BEGIN PLOT /ATLAS_2017_I1627873/d02-x01-y01
Title=
YLabel=$\sigma_{Zjj}^\textrm{fid}$ [fb]
XCustomMajorTicks=1 Baseline 2 High~$p_\mathrm{T}$ 3 EW-enriched 4 QCD-enriched 5 High-mass 6 High-mass,EW-enriched
XMajorLabelOrientation=20
XMajorTickLabelSeparation=4
BottomMargin=2.0
RightMargin=2.0
# END PLOT


# BEGIN PLOT /ATLAS_2017_I1627873/d03-x01-y01
Title=
YLabel=$\sigma_{Zjj}^\textrm{fid}$ [fb]
XCustomMajorTicks=1 EW-enriched 2 High-mass,EW-enriched
XMajorLabelOrientation=20
XMajorTickLabelSeparation=4
BottomMargin=2.0
RightMargin=2.0
# END PLOT
