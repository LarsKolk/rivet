Name: ATLAS_2016_I1424838
Year: 2015
Summary: Event shapes in leptonic $Z$-events
Experiment: ATLAS
Collider: LHC
InspireID: 1424838
Status: VALIDATED
Reentrant: true
Authors:
 - Holger Schulz <hschulz@cern.ch>
References:
 - ATLAS-STDM-2014-07
 - arXiv:1602.08980 [hep-ex]
 - Eur. Phys. J. C 76(7), 1-40, (2016)
RunInfo:
   $Z \to \ell\ell$ with $p_\perp(\ell)>20$~GeV
NumEvents: 1000000
NeedCrossSection: no
Beams: [p+, p+]
Energies: [7000]
Description:
  'Measurement of transverse event-shape observables ($N_\text{ch}$,
  $\sum \pT$, thrust, beam-thrust, $F$-parameter, and spherocity)
  in $Z \to \ell\ell$ events at 7~TeV for different $\pT(Z)$ regions.'
BibKey: Aad:2016ria
BibTeX: '@article{Aad:2016ria,
      author         = "Aad, Georges and others",
      title          = "{Measurement of event-shape observables in $Z \to
                        \ell^+ \ell^-$ events in $pp$ collisions at
                        $\sqrt{s}=7$~\TeV with the ATLAS detector at the LHC}",
      collaboration  = "ATLAS",
      year           = "2016",
      eprint         = "1602.08980",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "CERN-EP-2016-015",
      SLACcitation   = "%%CITATION = ARXIV:1602.08980;%%"
}'

ReleaseTests:
 - $A LHC-Z-e
 - $A-2 LHC-Z-mu

