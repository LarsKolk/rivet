BEGIN PLOT /BESIII_2016_I1484158/d01-x01-y01
Title=$\eta\pi^0$ mass distribution in $J/\psi\to \gamma\eta\pi^0$
XLabel=$m_{\eta\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
