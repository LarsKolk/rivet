BEGIN PLOT /BESIII_2020_I1837725/d01-x01-y01
Title=$\sigma(e^+e^-\to 2p2\bar{p})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to 2p2\bar{p})$/fb
ConnectGaps=1
END PLOT
