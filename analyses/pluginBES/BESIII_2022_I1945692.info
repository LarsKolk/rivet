Name: BESIII_2022_I1945692
Year: 2022
Summary: Dalitz decay of $D^+_s\to K^0_SK^0_S\pi^+$
Experiment: BESIII
Collider: BEPC
InspireID: 1945692
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 105 (2022) 5, L051103
RunInfo: Any process producing D_s+ mesons
Description:
  'Measurement of the mass distributions in the decay $D^+_s\to K^0_SK^0_S\pi^+$ by BES. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events using model in the paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021anf
BibTeX: '@article{BESIII:2021anf,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Study of the decay $D_s^+\to K_S^0K_S^0\pi^+$ and observation an isovector partner to $f_0(1710)$}",
    eprint = "2110.07650",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.105.L051103",
    journal = "Phys. Rev. D",
    volume = "105",
    number = "5",
    pages = "L051103",
    year = "2022"
}
'
