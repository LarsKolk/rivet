BEGIN PLOT /BESIII_2022_I1997940/d01-x01-y01
Title=$\eta^\prime\pi^+\pi^-$ mass distribution in $J/\psi\to e^+e^-\eta^\prime\pi^+\pi^-$ ($\eta^\prime\to \gamma\pi^+\pi^-$)
XLabel=$m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I1997940/d01-x01-y02
Title=$\eta^\prime\pi^+\pi^-$ mass distribution in $J/\psi\to e^+e^-\eta^\prime\pi^+\pi^-$ ($\eta^\prime\to \eta\pi^+\pi^-$)
XLabel=$m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
