BEGIN PLOT /LHCB_2012_I1114753/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $\Lambda_b^{*0}(5920)\to \Lambda_b^0\pi^+\pi^-$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [MeV]
LogY=0
END PLOT
