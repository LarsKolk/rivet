BEGIN PLOT /BABAR_2012_I1123662/d01-x01-y01
Title=Photon energy spectrum for $\bar{B}\to X_{s,d}\gamma$
XLabel=$E_\gamma$ [GeV]
YLabel=$\frac{1}{\Gamma_B}\frac{\text{d}\Gamma}{\text{d}E_\gamma}$ [ $10^{-5}\times\text{GeV}^{-1}$]
LogY=0
END PLOT
