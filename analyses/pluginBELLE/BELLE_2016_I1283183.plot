BEGIN PLOT /BELLE_2016_I1283183/d02-x01-y01
Title=Forward-Backward asymmetry in $B\to X_s\ell^+\ell^-$
XLabel=$q^2$ [$\text{GeV}^2$]
YLabel=$A_{FB}$
LogY=0
END PLOT
