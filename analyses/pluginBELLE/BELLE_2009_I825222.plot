BEGIN PLOT /BELLE_2009_I825222/d01-x01-y01
Title=$B\to X_s\gamma$ branching ratio vs photon energy cut
XLabel=$E_{\text{cut}}$ [GeV]
YLabel=Branching Ratio
END PLOT
BEGIN PLOT /BELLE_2009_I825222/d01-x01-y02
Title=$B\to X_s\gamma$ $\langle E_\gamma\rangle$ vs photon energy cut
XLabel=$E_{\text{cut}}$ [GeV]
YLabel=$\langle E_\gamma\rangle$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2009_I825222/d01-x01-y03
Title=$B\to X_s\gamma$ $\langle E^2_\gamma\rangle-\langle E_\gamma\rangle^2$ vs photon energy cut
XLabel=$E_{\text{cut}}$ [GeV]
YLabel=$\langle E^2_\gamma\rangle-\langle E_\gamma\rangle^2$ [$\text{GeV}^2$]
END PLOT
